import { environment } from "src/environments/environment"

const base_url = environment.base_url

export class User {
    constructor( 
        public google: boolean,
        public status: boolean,
        public date: Date,
        public name: string,
        public email: string,
        public password?: string,
        public img?: string,
        public role?: string,
        public id?: string) 
    {
       
    }
    get urlImg () {
        if (this.img){
            if (this.img.includes('https')){ // verificar si el usuario es de google y tiene img
                return this.img
            }
            return `${base_url}/api/uploads/users/${this.img}`
        }
        return `${base_url}/api/uploads/users/not-image`
    }
}