import { Hospital } from "./hospital.model";
import { User } from "./user.model";

export class Medical {

    constructor(
        public id?: string,
        public name?: string,
        public img?: string,
        public hospital?: Hospital,
        public status?: boolean,
        public user?: User
    ) {}

}

