import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Medical } from '../models/medico.model';

const base_url = environment.base_url

@Injectable({
  providedIn: 'root'
})
export class MedicalService {

  constructor( private http: HttpClient ) { }

  get token(): string {
    return localStorage.getItem('token') || '';
  }


  getMedicals() {
    const url = `${ base_url }/api/medicals`;
    return this.http.get( url )
              .pipe(
                map( (resp: {ok: boolean, medicals: Medical[] }) => resp.medicals )
              );
  }



  /**
   * 
   * @param id 
   * @returns 
   */
  getMedicalById(id:string) {
    const url = `${ base_url }/api/medicals/${id}`;
    return this.http.get( url )
              .pipe(
                map( (resp: {ok: boolean, medical: Medical }) => resp.medical )
              );
  }


  /**
   * 
   * @param name 
   * @returns 
   */
  createMedical( medical:Medical ) {
    const url = `${ base_url }/api/medicals`;
    return this.http.post( url, medical );
  }
  

  /**
   * 
   * @param medico 
   * @returns 
   */
  updateMedical( medico:Medical ) {

    const url = `${ base_url }/api/medicals/${ medico.id }`;
    return this.http.put( url, medico );
  }


  /**
   * 
   * @param _id 
   * @returns 
   */
  deleteMedical( _id: string ) {

    const url = `${ base_url }/api/medicals/${ _id }`;
    return this.http.delete( url );
  }
}
