import { Injectable, EventEmitter } from '@angular/core';
import { Upload } from '../shared/types/main';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  private _hideModal: boolean = true;
  public type: Upload;
  public id: string;
  public img: string;
  
  public newImage: EventEmitter<string> = new EventEmitter<string>();
  constructor() { }

  get hideModal() {
    return this._hideModal;
  }

  openModal( 
      tipo: Upload,
      id: string,
      img: string = 'not-image'
    ) {
      this._hideModal = false;
      this.type = tipo;
      this.id = id;
      this.img = img
  }

  cerrarModal() {
    this._hideModal = true;
  }

}
