import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  private theme = document.getElementById('theme');

  constructor() {
    this.theme.setAttribute('href', localStorage.getItem('theme') || './assets/css/colors/purple-dark.css')
   }

   checkLatestSelected(){
     // get check lastest selected element
     const classLink = localStorage.getItem('classLink')
     if (classLink) {
       document.querySelector(`.${classLink}-theme`).classList.add('working')
     }
   }

  changeTheme(theme: string) {

    const url = `./assets/css/colors/${theme}.css`
    this.theme.setAttribute('href', url)
    localStorage.setItem('theme', url)

    this.selectCurrentTheme(theme)
  }

  selectCurrentTheme(theme: string) {

    const links = document.querySelectorAll('.selector')

    links.forEach(link => {
      link.classList.remove('working') // clean class working

      const linkTheme = link.getAttribute('data-theme')

      if (linkTheme == theme) {
        link.classList.add('working')
        localStorage.setItem('classLink', linkTheme)
      }
    })
  }


}
