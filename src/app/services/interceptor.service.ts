import { HttpErrorResponse, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor{

  constructor() { }


  get headers() {
    return new HttpHeaders({
      'x-token': localStorage.getItem("token") || ''
    })
  
  }

  /**
   * Execute after of call service to backend, then we can inject validations, 
   * headers and handle errors
   * @param req 
   * @param next 
   * @returns 
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const reqClone = req.clone({
      headers:this.headers
    })
    return next.handle(reqClone).pipe(
      catchError( error => {
        console.warn("error in the request, please verify the methods.")
        return throwError(error)
      }))
  }
}
