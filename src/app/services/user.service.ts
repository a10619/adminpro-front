import { HttpClient } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { UploadUser as loadUser } from '../interfaces/cargar-usuarios.interface';
import { User } from '../models/user.model';
import { ADMIN_ROLE } from '../shared/helpers/vars';
import { SidebarService } from './sidebar.service';

const base_url = environment.base_url
declare const gapi: any

@Injectable({
  providedIn: 'root'
})

// >
// tap ; nos da una funcionalidad extra de poder obtener la respuesta del servidor aqui en el service
export class UserService {

  public auth2: any
  public user: User

  constructor(private http: HttpClient, 
              private ngZone: NgZone, 
              private router: Router,
              private sidebarServ: SidebarService) {
    this.googleInit()

  }

  get token(): string {
    return localStorage.getItem("token") || ''
  }

  get idUser(): string {
    return this.user.id
  }


  get userIsAdmin(): boolean{
    
    return this.user.role === ADMIN_ROLE
  }


  setUser(user:User){
    this.user = user

  }


  googleInit() {
    return new Promise(resolve => {
      gapi.load('auth2', (res) => {
        // Retrieve the singleton for the GoogleAuth library and set up the client.
        this.auth2 = gapi.auth2.init({
          client_id: '318868390003-drcgspvnahqf81391suqqr5g5k88q1e5.apps.googleusercontent.com',
          cookiepolicy: 'single_host_origin',
        });

        resolve(true);
      }, (err => console.log(err)));
    })
  }

  
  logout() {
    localStorage.removeItem("token")

    this.auth2.signOut().then(() => {
      this.ngZone.run(() => { // sirve oara ejecutar funciones de Angular dentro de otras libreriasW
        this.router.navigateByUrl('/login')
      })
    });

  }


  /**
   * 
   * @param token 
   * @returns 
   */
  loginGoogle(token: string) {
    return this.http.post(`${base_url}/api/login/google`, { token })
      .pipe(tap((res: any) => {
        const { user, sidebar } = res

        this.sidebarServ.menu = sidebar

        this.setUser(user)
        localStorage.setItem("token", res.token)
      })
      )
  }


  /**
   * 
   * @param data 
   * @returns 
   */
  loginUser(data: any) {
    return this.http.post(`${base_url}/api/login`, data)
      .pipe(tap((res: any) => {
        
        const { user, sidebar } = res
        this.sidebarServ.menu = sidebar
        
        this.setUser(user)
        localStorage.setItem("token", res.token)
      })
      )
  }



  validateToken(): Observable<boolean> {
    
    return this.http.get(`${base_url}/api/login/rnvtoken`,{
        headers: { 'x-token': this.token }
      }).pipe(map((res: any) => {

        const {user, sidebar } = res

        const { name, email, status, role, google, date, img = "", id } = user
        this.user = new User(google, status, date, name, email, "", img, role, id)
        
        if (!res.token) {
          localStorage.removeItem("token")
          return false
        } else {
          this.sidebarServ.menu = sidebar
          localStorage.setItem("token", res.token)
          return true
        }

        

      }),
        catchError(error => {
          localStorage.removeItem("token")
          
          return of(false)
        }))// catch error, sirve para manejar el error de la peticion. of = me retorna un observable: false
  }

  crearUsuario(data: any) {
    return this.http.post(`${base_url}/api/users`, data)
      .pipe(tap((res: any) => {
        localStorage.setItem("token", res.token)
      })
      )
  }


  actualizarPerfil(data: { name: string, email: string }): Observable<any> {
    return this.http.put(`${base_url}/api/users/${this.idUser}`, data)

  }


  eliminarUsuario(usuario: User) {
    // /usuarios/5eff3c5054f5efec174e9c84
    const url = `${base_url}/api/users/${usuario.id}`;
    return this.http.delete(url);
  }


  loadUsers(page: number = 0, _url: string = '') {

    let url = `${base_url}/api/users?page=${page}`;

    if (_url) url = _url

    return this.http.get<loadUser>(url)
      .pipe(
        map(resp => {
          // map users with properties of User interface
          let { total, previous, next, users } = resp

          users = users.map(
            user => new User(user.google, user.status, null, user.name, user.email, "", user.img, user.role, user.id)
          );
          return {
            total,
            users,
            previous,
            next
          };
        })
      )
  }


  updateUser(user: User) {
    return this.http.put(`${base_url}/api/users/${user.id}`, user);

  }
}
