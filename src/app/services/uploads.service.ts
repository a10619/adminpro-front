import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Upload } from '../shared/types/main';
const base_url = environment.base_url

@Injectable({
  providedIn: 'root'
})
export class UploadsService {

  constructor() { }

  async uploadImg(file: File, 
                      id:string,
                      tipo: Upload
  ){

    
    try {
      const url = `${base_url}/api/uploads/${tipo}/${id}`
      const formData = new FormData()
      formData.append('archivo', file)

      const res = await fetch(url, {
        method:"PUT",
        headers:{
          'x-token': localStorage.getItem("token") || ''
        },
        body: formData
      },);
      return await res.json();
    } catch (error) {
      console.error(error)
      return error
    }



  }
}
