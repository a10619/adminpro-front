import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Hospital } from '../models/hospital.model';

const base_url = environment.base_url;


@Injectable({
  providedIn: 'root'
})
export class HospitalService {


  constructor( private http: HttpClient ) { }

  get token(): string {
    return localStorage.getItem('token') || '';
  }




  getHospitals() {

    const url = `${ base_url }/api/hospitales`;
    return this.http.get( url )
              .pipe(
                map( (resp: {ok: boolean, hospitales: Hospital[] }) => resp.hospitales )
              );
  }

  crearHospital( data: Object ) {
    const url = `${ base_url }/api/hospitales`;
    return this.http.post( url, data );
  }
  
  actualizarHospital( hospital:any ) {

    const url = `${ base_url }/api/hospitales/${ hospital.id }`;
    return this.http.put( url, hospital );
  }

  borrarHospital( _id: string ) {

    const url = `${ base_url }/api/hospitales/${ _id }`;
    return this.http.delete( url );
  }

}
