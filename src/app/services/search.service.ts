import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { User } from '../models/user.model';
import { Upload } from '../shared/types/main';
import { Observable } from 'rxjs';
const base_url = environment.base_url;



@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor( private http: HttpClient ) { }

  get token(): string {
    return localStorage.getItem('token') || '';
  }


  /**
   * 
   * @param results search results
   * @returns 
   */
  private transformUsers( results: any[] ): User[] {
    return results.map(
      user => new User(user.google, user.status, null, user.name, user.email, user.password, user.img , user.role, user.id)  
    );
  }


  /**
   * 
   * @param value search value
   */

  searchAll(value: string): Observable<any>{
    const url = `${ base_url }/api/search/all/${ value }`;
    return this.http.get(url)
  }



  /**
   * 
   * @param type 
   * @param termino 
   * @returns 
   */
  search( 
      type: Upload,
      termino: string
    ) {

    const url = `${ base_url }/api/search/${ type }/${ termino }`;
    return this.http.get<any[]>( url )
            .pipe(
              map( (resp: any ) => { 

                switch ( type ) {
                  case 'users':
                    return this.transformUsers( resp.results )
                
                  default:
                    return resp;
                }

              })
            );

  }

}
