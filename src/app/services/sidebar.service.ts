import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Injectable, OnInit } from '@angular/core';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class SidebarService  {

  menu: any[] = [];
  
}
