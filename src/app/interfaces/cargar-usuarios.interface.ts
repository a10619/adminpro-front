import { User } from '../models/user.model';

export interface UploadUser {
    total: number;
    users: User[];
    previous:string;
    next:string;
}