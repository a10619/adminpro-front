import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Hospital } from 'src/app/models/hospital.model';
import { Medical } from 'src/app/models/medico.model';
import { User } from 'src/app/models/user.model';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, OnDestroy {


  public users: User[] = [];
  public medicals: Medical[] = [];
  public hospitals: Hospital[] = [];

  listenerParam: Subscription;

  constructor(private activatedRoute: ActivatedRoute,
    private searchServ: SearchService) { }

  ngOnInit(): void {

    this.listenerParam = this.activatedRoute.params.subscribe(({ value }) => {

      this.search(value)

    })
  }

  ngOnDestroy(): void {
    this.listenerParam.unsubscribe()
  }

  search(value: string) {
    this.searchServ.searchAll( value ).subscribe( (resp: any) => {
          this.users   = resp.users;
          this.medicals    = resp.medicals;
          this.hospitals = resp.hospitals;
        });
  }

}
