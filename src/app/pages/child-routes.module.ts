import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Grafica1Component } from './grafica1/grafica1.component';
import { UserGuard } from './maintenances/guards/user.guard';
import { HospitalsComponent } from './maintenances/pages/hospitals/hospitals.component';
import { MedicosComponent } from './maintenances/pages/medicos/medicos.component';
import { NewMedicalComponent } from './maintenances/pages/medicos/pages/new-medical/new-medical.component';
import { UsersComponent } from './maintenances/pages/users/users.component';
import { ProfileComponent } from './profile/profile.component';
import { ProgressComponent } from './progress/progress.component';
import { PromisesComponent } from './promises/promises.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { SearchComponent } from './search/search.component';



const childRoutes: Routes = [

  { path: '', component: DashboardComponent, data: {title: 'Dashboard'}},
  { path: 'profile', component: ProfileComponent, data: { title: 'My Profile'}},
  { path: 'progress', component: ProgressComponent, data: { title: 'ProgressBar' }},
  { path: 'grafica1', component: Grafica1Component, data: { title: 'Grafica 1' }},
  { path: 'account-settings', component: AccountSettingsComponent, data: { title: 'Settings theme' } },
  { path: 'promises', component: PromisesComponent, data: { title: 'Promises' }},
  { path: 'rxjs', component: RxjsComponent, data: { title: 'Rxjs' }},
  // mantenimientos
  {
    path: '',
    loadChildren: () => import('./maintenances/maintenances.module').then( m => m.MaintenancesModule)

  },
  { path: 'search/:value', component: SearchComponent, data: { title: 'Search results' }},

]


@NgModule({
  imports: [
      RouterModule.forChild(childRoutes)
  ],
  exports: [RouterModule]
})
export class ChildRoutesModule { }
