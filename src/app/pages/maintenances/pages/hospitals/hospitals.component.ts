import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Hospital } from 'src/app/models/hospital.model';
import { HospitalService } from 'src/app/services/hospital.service';
import { ModalService } from 'src/app/services/modal.service';
import { SearchService } from 'src/app/services/search.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-hospitals',
  templateUrl: './hospitals.component.html',
  styles: [
  ]
})
export class HospitalsComponent implements OnInit {

  public hospitales: Hospital[] = [];
  public loading: boolean = true;
  private imgSubs: Subscription;
  public cacheBusqueda:Hospital [] = []

  constructor( private hospitalService: HospitalService,
               private modalImagenService: ModalService) { }

  ngOnDestroy(): void {
    this.imgSubs.unsubscribe();
  }

  ngOnInit(): void {
    this.cargarHospitales();

    this.imgSubs = this.imgSubs = this.modalImagenService.newImage
      .pipe(delay(100))
      .subscribe( img => this.cargarHospitales() );
  }

  cargarHospitales() {

    this.loading = true;
    this.hospitalService.getHospitals()
        .subscribe( hospitales => {
          this.loading = false;
          this.hospitales = hospitales;
          this.cacheBusqueda = hospitales;
        })

  }

  actualizarHospital( hospital: Hospital ) {
    this.hospitalService.actualizarHospital(hospital)
        .subscribe( resp => {
          Swal.fire( 'Actualizado', hospital.name, 'success' );

        });

  }

  eliminarHospital( hospital: Hospital ) {
    Swal.fire({
      title: 'Do you sure want delete this?',
      showDenyButton: true,
      confirmButtonText: 'Delete',
      denyButtonText: `Cancel`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.hospitalService.borrarHospital( hospital.id )
        .subscribe( resp => {
          Swal.fire( 'Borrado', hospital.name, 'success' );
          this.cargarHospitales();
        });
      } else if (result.isDenied) {
        Swal.fire('Changes are not saved', '', 'info')
      }
    })


  }

  async abrirSweetAlert() {
    const { value = '' } = await Swal.fire<string>({
      title: 'Crear hospital',
      text: 'Ingrese el nombre del nuevo hospital',
      input: 'text',
      inputPlaceholder: 'Nombre del hospital',
      showCancelButton: true,
    });
    
    if( value.trim().length > 0 ) {
      this.hospitalService.crearHospital( { name: value } )
        .subscribe( (resp: any) => {
          this.cargarHospitales()
        })
    }
  }

  abrirModal(hospital: Hospital) {    
    this.modalImagenService.openModal( 'hospitals', hospital.id, hospital.img );
  }

  
  searchHospitales(termino: string){
    this.hospitales = this.cacheBusqueda.filter(
      h => h.name.toUpperCase().includes(termino.toUpperCase())  
    )

  }


}
