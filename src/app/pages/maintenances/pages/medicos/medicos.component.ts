import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Medical } from 'src/app/models/medico.model';
import { MedicalService } from 'src/app/services/medical.service';
import { ModalService } from 'src/app/services/modal.service';
import { SearchService } from 'src/app/services/search.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-medicos',
  templateUrl: './medicos.component.html',
  styles: [
  ]
})
export class MedicosComponent implements OnInit {

  public medicals: Medical[] = [];
  public loading: boolean = true;
  private imgSubs: Subscription;
  public cacheSearch: Medical [] = []

  constructor(private medicalService: MedicalService,
              private modalService: ModalService) { }

  ngOnDestroy(): void {
    this.imgSubs.unsubscribe();
  }

  ngOnInit(): void {
    this.getMedicals();

    this.imgSubs = this.modalService.newImage
      .pipe(delay(100))
      .subscribe( img => this.getMedicals() );
  }

  getMedicals() {

    this.loading = true;
    this.medicalService.getMedicals()
        .subscribe( medicals => {
          this.loading = false;
          this.medicals = medicals;
          this.cacheSearch = medicals;
        })

  }

  updateMedical( medical: Medical ) {
    this.medicalService.updateMedical(medical)
        .subscribe( resp => {
          Swal.fire( 'Updated', medical.name, 'success' );

        });
  }

  deleteMedical( medical: Medical ) {
    Swal.fire({
      title: 'Do you sure want delete this?',
      showDenyButton: true,
      confirmButtonText: 'Delete',
      denyButtonText: `Cancel`,

    }).then((result) => {

      if (result.isConfirmed) {
        this.medicalService.deleteMedical( medical.id )
        .subscribe( resp => {
          Swal.fire( 'Borrado', medical.name, 'success' );
          this.getMedicals();
        });
      } else if (result.isDenied) {
        Swal.fire('Changes are not saved', '', 'info')
      }
    })
  }


  openModal(medico: Medical) {    
    this.modalService.openModal( 'medicals', medico.id, medico.img );
  }


  searchMedicals(termino: string){
    this.medicals = this.cacheSearch.filter(
      h => h.name.toUpperCase().includes(termino.toUpperCase())  
    )

  }


}
