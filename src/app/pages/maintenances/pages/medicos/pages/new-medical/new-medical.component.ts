import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { delay } from 'rxjs/operators';
import { Hospital } from 'src/app/models/hospital.model';
import { Medical } from 'src/app/models/medico.model';
import { HospitalService } from 'src/app/services/hospital.service';
import { MedicalService } from 'src/app/services/medical.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-new-medical',
  templateUrl: './new-medical.component.html',
  styleUrls: ['./new-medical.component.css']
})
export class NewMedicalComponent implements OnInit {

  public medicoForm: FormGroup;
  public hospitals: Hospital[] = [];
  
  public hospitalSelected: Hospital;



  constructor(private fb: FormBuilder,
              private hospitalService: HospitalService,
              private medicalService: MedicalService,
              private router: Router) { }

  ngOnInit(): void {

    this.medicoForm = this.fb.group({
      name: ['', Validators.required ],
      hospital: ['', Validators.required ],
    });

    this.getHospitals();

    this.medicoForm.get('hospital').valueChanges
        .subscribe( hospitalId => {
          this.hospitalSelected = this.hospitals.find( h => h.id=== hospitalId );
        })
  }


  getHospitals() {

    this.hospitalService.getHospitals()
      .subscribe( (hospitals: Hospital[]) => {
        this.hospitals = hospitals;
      })

  }

  saveMedical() {

    const {name} = this.medicoForm.value;

    this.medicalService.createMedical( this.medicoForm.value )
        .subscribe( (resp: any) => {
          Swal.fire('Created', `${ name } successfully`, 'success');
          this.router.navigateByUrl(`/dashboard/medicals`)
      })
      
    



  }

}
