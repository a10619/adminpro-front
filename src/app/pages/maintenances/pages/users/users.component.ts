import { Component, OnInit, OnDestroy } from '@angular/core';
import Swal from 'sweetalert2';
import { delay } from 'rxjs/operators';
import { User } from '../../../../models/user.model';
import { ModalService } from 'src/app/services/modal.service';
import { SearchService } from '../../../../services/search.service';
import { UserService } from '../../../../services/user.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styles: [
  ]
})
export class UsersComponent implements OnInit {

  public totalUsuarios: number = 0;
  public users: User[] = [];
  public usersTemp: User[] = [];
  public next: string;
  public previous:string;
  public imgSubs: Subscription;
  public desde: number = 0;
  public loading: boolean = true;

  constructor(private userService: UserService,
              private busquedasService: SearchService,
              private modalService: ModalService ) { }
  
  ngOnDestroy(): void {
    // ng destroy evitar fugas de memoria, es decir no volver a ejecutar el servicio
    this.imgSubs.unsubscribe();
  }

  ngOnInit(): void {
    this.loadUsers()
    // nos suscribimos para cuando se reciba un emit, ejecute la funcion loadUsers()
    this.imgSubs = this.modalService.newImage
      .pipe(delay(100))
      .subscribe( img => this.loadUsers() );
  }

  loadUsers(url:string = '') {
    this.loading = true;
    this.userService.loadUsers( this.desde, url )
      .subscribe( ({ total, users, next, previous }) => {
        this.totalUsuarios = total;
        this.users = users;
        this.usersTemp = users;
        this.next = next;
        this.previous = previous;
        this.loading = false;
    })
  }

  cambiarPagina( valor: string ) {
    if (valor == 'previous' && this.previous != null){
      this.loadUsers(this.previous)
    } else if ( valor == 'next' && this.next != null){
      this.loadUsers(this.next)
    }
  }

  buscar( termino: string ) {
    if ( termino.length === 0 ) {
      return this.users = this.usersTemp;
    }

    this.busquedasService.search( 'users', termino )
        .subscribe( resp => {
          this.users = resp;

        });
  }

  eliminarUsuario( usuario: User ) {

    if ( usuario.id === this.userService.idUser ) {
      return Swal.fire('Error', 'No puede borrarse a si mismo', 'error');
    }

    Swal.fire({
      title: '¿Borrar usuario?',
      text: `Esta a punto de borrar a ${ usuario.name }`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Si, borrarlo'
    }).then((result) => {
      if (result.value) {
        
        this.userService.eliminarUsuario( usuario )
          .subscribe( resp => {
            
            Swal.fire(
              'Usuario borrado',
              `${ usuario.name } fue eliminado correctamente`,
              'success'
              );
            this.loadUsers();
            
          }, err => {
            console.error(err)
          })

      }
    })

  }

  cambiarRole( usuario:User ) {
    
    this.userService.updateUser( usuario )
      .subscribe( () => {}, err => {
        Swal.fire('Error', 'Hubo un error al actualizar el rol', 'error');
        console.error(err)
      })
  }


  abrirModal( usuario: User ) {
    this.modalService.openModal('users', usuario.id, usuario.urlImg );
  }


  updateUser(user:User){
    this.userService.updateUser(user)
        .subscribe( resp => {
          Swal.fire( 'Updated', user.name, 'success' );

        });
  }


}
