import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './pages/users/users.component';
import { HospitalsComponent } from './pages/hospitals/hospitals.component';
import { MedicosComponent } from './pages/medicos/medicos.component';
import { FormsModule } from '@angular/forms';
import { PipeModule } from 'src/app/pipes/pipe.module';
import { MaintenancesRouting } from './maintenances.routing';



@NgModule({
  declarations: [
    UsersComponent,
    HospitalsComponent,
    MedicosComponent
  ],
  imports: [
    CommonModule,
    MaintenancesRouting,
    FormsModule,
    PipeModule
  ]
})
export class MaintenancesModule { }
