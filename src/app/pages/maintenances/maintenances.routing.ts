import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserGuard } from './guards/user.guard';
import { HospitalsComponent } from './pages/hospitals/hospitals.component';
import { MedicosComponent } from './pages/medicos/medicos.component';
import { NewMedicalComponent } from './pages/medicos/pages/new-medical/new-medical.component';
import { UsersComponent } from './pages/users/users.component';

const routes: Routes = [
  {
    path: 'users',
    component: UsersComponent,
    data: { title: 'Users' },
    canActivate: [UserGuard]
  },
  { path: 'hospitals', component: HospitalsComponent, data: { title: 'Hospitals' } },
  { path: 'medicals', component: MedicosComponent, data: { title: 'Medicals' } },
  { path: 'medicals/new', component: NewMedicalComponent, data: { title: 'Create medical' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaintenancesRouting { }
