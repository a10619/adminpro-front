import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { UploadsService } from 'src/app/services/uploads.service';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styles: [
  ]
})
export class ProfileComponent implements OnInit {

  profileForm: FormGroup
  extensions_allowed = ['jpg', 'png', 'jpeg', 'webp']
  user: User
  imageUser: File
  errorMessage: string = ""
  imgTemp: any = null
  
  constructor(private fb: FormBuilder, 
              private userService: UserService, 
              private uploadService: UploadsService) { 
    this.user = this.userService.user
  }

  ngOnInit(): void {
    this.profileForm = this.fb.group({
      name: [this.user.name, Validators.required],
      email:[this.user.email , [     
        Validators.required,
        Validators.pattern(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
       ]
      ],
    })
  }

  validateImg(name:string): boolean{
    const arr_name = name.split('.')
    const ext = arr_name[arr_name.length - 1]

    if (this.extensions_allowed.includes(ext)){
      return true
    }
    return false
  }

  changeImg(file:File){
    this.imageUser = file;
    if (!file) return this.imgTemp = null;

    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onloadend = () => {
      this.imgTemp = reader.result
    }
  }

  updateProfile(){

    this.userService.actualizarPerfil(this.profileForm.value).subscribe( res => {
      this.errorMessage = ""
      const { user }  = res
      this.user.name  = user.name
      this.user.email = user.email

      Swal.fire(
        'Saved!',
        '',
        'success'
      )
    }, (err => {
      this.errorMessage = err.error.msg
      console.error(err.error.msg)
    }))
  }

  actualizarImagen(){
    if (this.validateImg(this.imageUser.name)){
      this.uploadService.uploadImg(this.imageUser,  this.user.id, "users").then( res => {
        const {ok, msg} = res
        if (ok){
          Swal.fire('Imagen actualizada', '', 'success')
          this.user.img = res.data
        } else {
          console.error(res)
          Swal.fire('Error', msg , 'error')
        }
      }).catch( err => {
        console.error(err)
        Swal.fire('Error', 'No se pudo subir la imagen', 'error')
      })
    } else {
      Swal.fire("Archivo no permitido", "", "error")
    }
  }

}
;