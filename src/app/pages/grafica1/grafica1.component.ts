import { Component } from '@angular/core';


@Component({
  selector: 'app-grafica1',
  templateUrl: './grafica1.component.html',
  styles: [
  ]
})
export class Grafica1Component {
  label1 = ['Moto Pulsar', 'PS5', 'XBOX'];
  label2 = ['Creme Care','Arroz', 'Pescado']
  data1  = [
    [120, 250, 120]
  ];

  data2 = [
    [300, 150, 130]
  ];
  
}
