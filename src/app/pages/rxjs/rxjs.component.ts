import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, interval, Subscription } from 'rxjs';
import { retry, take, map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: [
  ]
})
export class RxjsComponent implements OnDestroy  {

  public intervalSubs: Subscription

  constructor() { 
    
    // // pipe => transforma la informacion que fluye dentro del observable
    // this.retornarObservable().pipe(
    //   retry(3)
    // ).subscribe( 
    //   value => console.log(value),
    //   error => console.error("Error ", error),
    //   () => console.info("Observable terminado! "),
    //   )
    this.intervalSubs = this.retornaInterval()
        .subscribe( value => console.log(value))
  }
  ngOnDestroy(): void {
    this.intervalSubs.unsubscribe(); // unsubscribe observable
  }

  retornaInterval(){
    return interval(500)
          .pipe(
            take(10), // definimos la cantidad de intervalos
            map(value => value + 1),
            filter( value => value % 2 === 0),
            
            
            )
  }
  retornarObservable(): Observable<number>{
    let i = 0;
    
    return new Observable<number>(observer => {
      const intervalo = setInterval(() => {
        i++;
        observer.next(i); // emitimos el valor de nuestro observador

        if (i === 5) {
          clearInterval(intervalo); // limpiamos el intervalo
          observer.complete() // completamos nuestro observable
        }
        if (i === 2) {
          observer.error("error en linea 24") // emit error
        }
      }, 1000)
    })
  }

}
