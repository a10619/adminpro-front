import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesRoutingModule } from './pages/pages.routing';

import { NotpagefoundComponent } from './notpagefound/notpagefound.component';
import { AuthRoutingModule } from './auth/auth.routing';
import { DashboardComponent } from './pages/dashboard/dashboard.component';


const routes: Routes = [  
  { 
    path: '', 
    component: DashboardComponent,
    pathMatch: 'full',
  },
  { path: '**', component: NotpagefoundComponent}
]

@NgModule({
  imports: [
    RouterModule.forRoot( routes ),
    // routes modules
    PagesRoutingModule,
    AuthRoutingModule,
    


  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
