import { Component, Input, OnInit } from '@angular/core';
import { MultiDataSet, Label, Color } from 'ng2-charts';

@Component({
  selector: 'app-dona',
  templateUrl: './dona.component.html',
  styles: [
  ]
})
export class DonaComponent implements OnInit{

  @Input() title: string = "Name default";
  @Input() labels: Label[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
  @Input() chartData: MultiDataSet = [
    [120, 250, 100]
  ];
  colors: Color[] = [
    {
      backgroundColor: ['#0097a7', '#ff5131', '#FFB414']
    }
  ]
  ngOnInit(): void {

  }
  
}
