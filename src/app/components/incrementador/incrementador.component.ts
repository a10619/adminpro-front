import { Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-incrementador',
  templateUrl: './incrementador.component.html',
})

export class IncrementadorComponent {

  @Input('value') progress: number = 80;
  @Input() btnClass: string = 'btn btn-primary';

  @Output() emitirCambio:EventEmitter<number> = new EventEmitter();

  constructor() { }

  get getPorcentaje(): string {
    return `${this.progress}%`
  }

  cambiarPocentaje(valor: number) {

    this.progress += valor

    if (this.progress >= 100) this.progress = 100

    if (this.progress <= 0) this.progress = 0

    this.emitirCambio.emit(this.progress)

  }

  onChange(valor: any){
    if (valor >= 100) {
      this.progress = 100
    }
    else if (valor <= 0) {
      this.progress = 0
    } else{
      this.progress = valor
    }

    this.emitirCambio.emit(this.progress)
  }
}
