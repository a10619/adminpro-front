import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IncrementadorComponent } from './incrementador/incrementador.component';
import { FormsModule } from '@angular/forms';
import { DonaComponent } from './dona/dona.component';
import { ChartsModule } from 'ng2-charts';
import { ModalImgComponent } from './modal-img/modal-img.component';
import { PipeModule } from '../pipes/pipe.module';



@NgModule({
  declarations: [
    IncrementadorComponent,
    DonaComponent,
    ModalImgComponent
  ],

  exports: [
    IncrementadorComponent,
    DonaComponent,
    ModalImgComponent
  ],
  
  imports: [
    CommonModule,
    FormsModule,
    PipeModule,
    // terceros
    ChartsModule,
  ]
})
export class ComponentsModule { }
