import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

import { UploadsService } from '../../services/uploads.service';
import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'app-modal-img',
  templateUrl: './modal-img.component.html'
})
export class ModalImgComponent implements OnInit {

  public imagenSubir: File;
  public imgTemp: any = null;

  constructor( public modalService: ModalService,
               public fileUploadService: UploadsService  ) { }

  ngOnInit(): void {
  }


  cerrarModal() {
    this.imgTemp = null;
    this.modalService.cerrarModal();
  }

  cambiarImagen( file: File ) {
    this.imagenSubir = file;

    if ( !file ) { 
      return this.imgTemp = null;
    }

    const reader = new FileReader();
    reader.readAsDataURL( file );

    reader.onloadend = () => {
      this.imgTemp = reader.result;
    }

  }

  subirImagen() {

    const id   = this.modalService.id;
    const tipo = this.modalService.type;

    this.fileUploadService
      .uploadImg( this.imagenSubir, id, tipo )
      .then( img => {
        Swal.fire('Guardado', 'Imagen de usuario actualizada', 'success');
        // emit change img
        this.modalService.newImage.emit(img);

        this.cerrarModal();
      }).catch( err => {
        console.error(err);
        Swal.fire('Error', 'No se pudo subir la imagen', 'error');
      })

  }

}
