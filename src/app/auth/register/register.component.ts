import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {

  submittedForm:boolean = false 
  msgError: string = ''

  registerForm = this.fb.group({
    name:['', [Validators.required, Validators.minLength(3)]],
    email:['jei@gmail.com', [
      Validators.required,
      Validators.pattern(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
     ]
    ],
    password:['', [Validators.required]],
    password2:['', [Validators.required ]],
    terminos:[false, [Validators.required, this.validarTerminos]]
  }, {
    validators: this.passwordsIguales()
  })

  constructor(private fb: FormBuilder, 
              private router: Router,
              private userService: UserService) { }

  createUser() {
    this.submittedForm = true

    const {email , name, password } = this.registerForm.value 
    this.userService.crearUsuario({email, name, password })
    .subscribe( (res) => {

      // mover al dashboard
      this.router.navigateByUrl('/')
    }, (err) => {
      console.error(err)
      this.msgError = err.error.errors.email.msg
    })
  }

  ngOnInit(): void {
  }

  validarTerminos(form) {

    if (!form.value) {
      return {invalid: false  }
    } else {
      return null
    }
  }

  passwordsIguales() {

    return ( formGroup: FormGroup) => {

      const pass1 = formGroup.get("password").value
      const pass2 = formGroup.get("password2").value

      if ((pass2.length && pass1.length) >  0) {
        if (pass2 === pass1 ){
          formGroup.get("password2").setErrors(null)
        } else {
          formGroup.get("password2").setErrors({noEsIgual: true}) 
           
       } 
      } else {
        formGroup.get("password2").setErrors(null)

      }


      

    }

  }

}
