import { Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

declare const gapi:any

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.css']
})
export class LoginComponent implements OnInit {
  msgError: string = ''
  auth2: any
  loginForm = this.fb.group({
    email:[ localStorage.getItem("email") || '' , [     
      Validators.required,
      Validators.pattern(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
     ]
    ],
    password:['', [Validators.required ]],
    remember:[ localStorage.getItem("email") ? true : false]
  })

  constructor(  private router: Router, 
                private fb: FormBuilder, 
                private userService: UserService,
                private ngZone:NgZone) { }

  ngOnInit(): void {
    this.renderButton()
  }

  login(){
    this.userService.loginUser(this.loginForm.value)
        .subscribe( res => {

          if (this.loginForm.value.remember) {
            localStorage.setItem("email", this.loginForm.value.email)
          } else {
            localStorage.removeItem("email")
          }
          
          // mover al dashboard
          this.router.navigateByUrl('/')
        }, (err) => {
          this.msgError = 'Correo o contraseña incorrectos!'
        })


  }

  renderButton() {
    gapi.signin2.render('my-signin2', {
      'scope': 'profile email',
      'width': 240,
      'height': 50,
      'longtitle': true,
      'theme': 'dark'
    });

    this.startApp()
  }

  async startApp() {

    await this.userService.googleInit()
    this.auth2 = this.userService.auth2
    this.attachSignin(document.getElementById('my-signin2'));

  }

  attachSignin(element) {

    this.auth2.attachClickHandler(element, {},
      (googleUser) => {
        const id_token = googleUser.getAuthResponse().id_token
        this.userService.loginGoogle(id_token).subscribe(res => {

          // mover al dashboard
          this.ngZone.run(() => {
            this.router.navigateByUrl('/')
          })
        })

      }, (error) => {
        console.error(error)
        });
  
  }


}
