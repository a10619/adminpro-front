import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route, UrlSegment } from '@angular/router';
import { UserService } from '../../services/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

  constructor(private router: Router, 
              private userService: UserService,
  ) { }
  
  canLoad(route: Route, segments: UrlSegment[]): Promise<boolean>{

    return new Promise( resolve => {
      this.userService.validateToken().subscribe( isAuth => {
        if(route.path !== 'login'  && !isAuth){
          this.router.navigateByUrl('/login')
          resolve(isAuth)
          return isAuth
        } else {
          resolve(isAuth)
          return isAuth
        }  
      })
    })

} 

    
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean>  {
      return new Promise( resolve => {
        this.userService.validateToken().subscribe( isAuth => {
          if(route.routeConfig.path !== 'login'  && !isAuth){
            this.router.navigateByUrl('/login')
            resolve(isAuth)
            return
          } else {
            resolve(isAuth)
          }  
        })
      })

  } 

}