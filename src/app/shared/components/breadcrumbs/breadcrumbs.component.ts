import { Component, OnDestroy } from '@angular/core';
import { ActivationEnd, NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styles: [
  ]
})
export class BreadcrumbsComponent implements OnDestroy{
  
  breadcrumbs:any[] = [];
  currTtitle: string = '';
  titleSubs$: Subscription;

  constructor(private router: Router) {
    this.listenerRouteChanges()
  }

  ngOnDestroy(): void {
    this.titleSubs$.unsubscribe() // unsubscribe from getNameRouteCurrency
  }



  listenerRouteChanges() {

    this.titleSubs$ = this.router.events.subscribe((event) => {

      if (event instanceof NavigationEnd) {
        this.breadcrumbs = event.url.split('/')
        this.breadcrumbs = this.breadcrumbs.map((b, i) => {

          if (this.breadcrumbs[i - 1]) {
            return {
              url: `/${this.breadcrumbs[i - 1]}/${this.breadcrumbs[i]}`,
              name: b
            }
          } else {
            return {
              url: `/${this.breadcrumbs[i]}`,
              name: b
            }
          }
        })

        // rm first and last element the array
        this.breadcrumbs = this.breadcrumbs.slice(1, -1)
      }

      if (event instanceof ActivationEnd){
        if (event.snapshot.data.title){
          this.currTtitle = event.snapshot.data.title
        }
      }
    });
  }

}
