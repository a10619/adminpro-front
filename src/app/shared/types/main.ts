type Upload = 'users' | 'hospitals' | 'medicals';

type Role = 'ROLE_USER' | 'ROLE_ADMIN'

export {Upload, Role}