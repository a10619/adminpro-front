import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Upload } from '../shared/types/main';

@Pipe({
  name: 'imagen'
})
export class ImagenPipe implements PipeTransform {

  transform(img: string, type: Upload): string {
    if (img){
      if (img.includes('https')){ // verificar si el usuario es de google y tiene img
          return img
      }

      if (type){
        return `${environment.base_url}/api/uploads/${type}/${img}`

      }
  }
    return `${environment.base_url}/api/uploads/users/no-image`
  }

}
